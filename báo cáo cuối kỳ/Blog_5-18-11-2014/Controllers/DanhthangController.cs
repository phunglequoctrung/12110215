﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5_18_11_2014.Models;

namespace Blog_5_18_11_2014.Controllers
{
    public class DanhthangController : Controller
    {
        private BlogDBContext db = new BlogDBContext();

        //
        // GET: /Danhthang/

        public ActionResult Index()
        {
            var danhthangs = db.Danhthangs.Include(d => d.UserProFile);
            return View(danhthangs.ToList());
        }

        //
        // GET: /Danhthang/Details/5

        public ActionResult Details(int id = 0)
        {
            Danhthang danhthang = db.Danhthangs.Find(id);
            if (danhthang == null)
            {
                return HttpNotFound();
            }
            ViewData["idpost"] = id;
            return View(danhthang);
        }

        //
        // GET: /Danhthang/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Danhthang/Create

        [HttpPost]
        public ActionResult Create(Danhthang danhthang, string content)
        {
            //if (ModelState.IsValid)
            //{
            //    db.Danhthangs.Add(danhthang);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            //ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", danhthang.UserProfileUserId);
            //return View(danhthang);
            if (ModelState.IsValid)
            {
                int userID = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;

                danhthang.DateCreated = DateTime.Now;
                // Tao list cac Tag
                List<Tag> Tags = new List<Tag>();
                // Tach cac tag theo dau ,
                string[] TagContent = content.Split(',');
                // Lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //Tim xem Tag Content da co hay chua
                    Tag TagExits = null;
                    var ListTag = db.Tags.Where(y => y.content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        // neu co tag roi thi them post vao 
                        TagExits = ListTag.First();
                        TagExits.Danhthangs.Add(danhthang);
                    }
                    else
                    {
                        // neu chua co tag thi tao moi
                        TagExits = new Tag();
                        TagExits.content = item;
                        TagExits.Posts = new List<Post>();
                        TagExits.Danhthangs.Add(danhthang);
                    }
                    //add vao List cac Tag
                    Tags.Add(TagExits);
                }
                //Gan list Tag cho Post
                danhthang.Tags = Tags;
                danhthang.UserProfileUserId = userID;
                db.Danhthangs.Add(danhthang);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", danhthang.UserProfileUserId);
            return View(danhthang);
        }

        //
        // GET: /Danhthang/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Danhthang danhthang = db.Danhthangs.Find(id);
            if (danhthang == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", danhthang.UserProfileUserId);
            return View(danhthang);
        }

        //
        // POST: /Danhthang/Edit/5

        [HttpPost]
        public ActionResult Edit(Danhthang danhthang)
        {
            if (ModelState.IsValid)
            {
                db.Entry(danhthang).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", danhthang.UserProfileUserId);
            return View(danhthang);
        }

        //
        // GET: /Danhthang/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Danhthang danhthang = db.Danhthangs.Find(id);
            if (danhthang == null)
            {
                return HttpNotFound();
            }
            return View(danhthang);
        }

        //
        // POST: /Danhthang/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Danhthang danhthang = db.Danhthangs.Find(id);
            db.Danhthangs.Remove(danhthang);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}