﻿using Blog_5_18_11_2014.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog_5_18_11_2014.Controllers
{
    public class HomeController : Controller
    
    {
        private BlogDBContext db = new BlogDBContext();

        public ActionResult Index()
        {
            ViewBag.posts = db.Posts.OrderByDescending(x => x.DateCreated).Take(8);
            return View(db.Tags.ToList());
        }

       /* public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }*/

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
