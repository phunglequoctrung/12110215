namespace Blog_5_18_11_2014.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Danhthangs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.DanhthangTags",
                c => new
                    {
                        Danhthang_ID = c.Int(nullable: false),
                        Tag_TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Danhthang_ID, t.Tag_TagID })
                .ForeignKey("dbo.Danhthangs", t => t.Danhthang_ID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .Index(t => t.Danhthang_ID)
                .Index(t => t.Tag_TagID);
            
            AddColumn("dbo.Comments", "DanhthangdtID", c => c.Int(nullable: false));
            AddColumn("dbo.Comments", "DanhThang_ID", c => c.Int());
            AddForeignKey("dbo.Comments", "DanhThang_ID", "dbo.Danhthangs", "ID");
            CreateIndex("dbo.Comments", "DanhThang_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.DanhthangTags", new[] { "Tag_TagID" });
            DropIndex("dbo.DanhthangTags", new[] { "Danhthang_ID" });
            DropIndex("dbo.Danhthangs", new[] { "UserProfileUserId" });
            DropIndex("dbo.Comments", new[] { "DanhThang_ID" });
            DropForeignKey("dbo.DanhthangTags", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.DanhthangTags", "Danhthang_ID", "dbo.Danhthangs");
            DropForeignKey("dbo.Danhthangs", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "DanhThang_ID", "dbo.Danhthangs");
            DropColumn("dbo.Comments", "DanhThang_ID");
            DropColumn("dbo.Comments", "DanhthangdtID");
            DropTable("dbo.DanhthangTags");
            DropTable("dbo.Danhthangs");
        }
    }
}
