namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts", "FistName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.Accounts", "FistName", c => c.String(nullable: false));
        }
    }
}
